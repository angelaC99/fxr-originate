
from anomalyreport.anomaly_report import clean_name_for_comparison, anomaly
from anomalyreport.conversion import object_creation_from_dataframe
from anomalyreport.report_generator import anomaly_report_generation, changelog_report_generation, \
    normalized_report_generation
from discrepancyreport import discrepancy_report as dr
from sheets import errors as err
from sheets import general

from fastapi import FastAPI, HTTPException

app = FastAPI()

@app.get("/")
def read_link():
    return {"Hello": "World"}


@app.get("/anomaly_report/")
def anomaly_report(*, sheet_index: int, link: str):
    try:
        google_client = general.authorizing()

        # Opening the spreadsheet
        spreadsheet = general.collect_spreadsheet(google_client, link)

        header, dataframe = general.parse_data(spreadsheet, sheet_index)

        # Making the list for organization
        sales_force_list = [object_creation_from_dataframe(index, row) for index, row in dataframe.iterrows()]
        normalized_org_list = [object_creation_from_dataframe(index, row) for index, row in dataframe.iterrows()]
        clean_name_for_comparison(sales_force_list)

        # Running the anomaly algorithm
        final_report = anomaly(header, sales_force_list, normalized_org_list)

        # Printing the anomaly report
        anomaly_report_generation(spreadsheet, final_report)

        # Printing the changelog
        changelog_report_generation(spreadsheet, final_report)

        # Printing the normalised data
        normalized_report_generation(spreadsheet, final_report)
        return {"Index": sheet_index, "Link": link, "Status": "Success"}
    except err.SheetsError as e:
        raise HTTPException(status_code=e.error_code, detail=e.error_message)


@app.get("/discrepancy_report/")
def discrepancy_report(*, fxr_index: int, sales_index: int, link: str):
    try:
        key = general.authorizing()

        worksheet = general.collect_spreadsheet(key, link)

        salesforce_report, fxrone_data = dr.getting_dataframes(fxr_index, sales_index, worksheet)

        salesforce_dataframe, fxrone_dataframe = dr.formatting_dataframes(salesforce_report, fxrone_data)

        concat_data, not_in_fxrone, not_in_salesforce = dr.creating_discrepancies(salesforce_dataframe, fxrone_dataframe)

        discrepancy_spreadsheet = dr.set_up_spreadsheet(worksheet, concat_data, not_in_fxrone, not_in_salesforce)

        dr.gen_header_details(discrepancy_spreadsheet)

        dr.gen_discrepancy(discrepancy_spreadsheet, concat_data)

        dr.gen_not_in_fxrone(discrepancy_spreadsheet, not_in_fxrone, concat_data)

        dr.gen_not_in_salesforce(discrepancy_spreadsheet, not_in_salesforce, concat_data, not_in_fxrone)
        return {"FXR Index": fxr_index, "Sales Index": sales_index, "Link": link, "Status": "Success"}
    except err.SheetsError as e:
        raise HTTPException(status_code=e.error_code, detail=e.error_message)


