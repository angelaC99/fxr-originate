import os.path

import pygsheets
import pytest

from sheets import errors as er
from discrepancyreport import discrepancy_report as dr, errors


def test_authorizing(monkeypatch):
    def mockreturn_dirname(path):
        return "/abc"

    def mockreturn_join(path, key):
        return "/abc/client_secret.json"

    def mockreturn_gc(key):
        return pygsheets.client.Client

    monkeypatch.setattr(os.path, "dirname", mockreturn_dirname)
    monkeypatch.setattr(os.path, "join", mockreturn_join)
    monkeypatch.setattr(pygsheets, "authorize", mockreturn_gc)
    x = dr.authorizing()
    assert x == pygsheets.client.Client

    def mockreturn_no_join(path, key):
        raise FileNotFoundError

    monkeypatch.setattr(os.path, "join", mockreturn_no_join)
    with pytest.raises(er.ClientFileNotFoundError):
        assert dr.authorizing()

    def mockreturn_no_gc(key):
        raise pygsheets.AuthenticationError

    monkeypatch.setattr(os.path, "join", mockreturn_join)
    monkeypatch.setattr(pygsheets, "authorize", mockreturn_no_gc)
    with pytest.raises(er.InvalidClientKey):
        assert dr.authorizing()

    def mockreturn_no_gc(key):
        raise pygsheets.SpreadsheetNotFound

    monkeypatch.setattr(os.path, "join", mockreturn_join)
    monkeypatch.setattr(pygsheets, "authorize", mockreturn_no_gc)
    with pytest.raises(errors.DiscrepancyReportError):
        assert dr.authorizing()

    def mockreturn_no_gc(key):
        raise ValueError

    monkeypatch.setattr(os.path, "join", mockreturn_join)
    monkeypatch.setattr(pygsheets, "authorize", mockreturn_no_gc)
    with pytest.raises(er.SheetsError):
        assert dr.authorizing()


def test_collect_spreadsheet(monkeypatch):
    link = "/////cool/"
    gc = pygsheets.client.Client

    def mockreturn_open_by_key(key):
        return pygsheets.spreadsheet

    monkeypatch.setattr(gc, "open_by_key", mockreturn_open_by_key)
    x = dr.collect_spreadsheet(gc, link)
    assert x == pygsheets.spreadsheet

    def mockreturn_failed_open_by_key(key):
        raise pygsheets.SpreadsheetNotFound

    monkeypatch.setattr(gc, "open_by_key", mockreturn_failed_open_by_key)
    with pytest.raises(er.CollectSpreadsheetError):
        assert dr.collect_spreadsheet(gc, link)

    def mockreturn_failed_open_by_key(key):
        raise pygsheets.NoValidUrlKeyFound

    monkeypatch.setattr(gc, "open_by_key", mockreturn_failed_open_by_key)
    with pytest.raises(er.SpreadsheetNoValidURLKey):
        assert dr.collect_spreadsheet(gc, link)

    def mockreturn_failed_open_by_key(key):
        raise pygsheets.AuthenticationError

    monkeypatch.setattr(gc, "open_by_key", mockreturn_failed_open_by_key)
    with pytest.raises(errors.DiscrepancyReportError):
        assert dr.collect_spreadsheet(gc, link)

    def mockreturn_failed_open_by_key(key):
        raise ValueError

    monkeypatch.setattr(gc, "open_by_key", mockreturn_failed_open_by_key)
    with pytest.raises(er.SheetsError):
        assert dr.collect_spreadsheet(gc, link)

    link = "///"
    with pytest.raises(er.IndexError):
        assert dr.collect_spreadsheet(gc, link)


def test_find_data_x(monkeypatch):
    google_spreadsheet = pygsheets.Spreadsheet
    index = 1

    def mockreturn_worksheet(str, num):
        return pygsheets.worksheet

    monkeypatch.setattr(google_spreadsheet, "worksheet", mockreturn_worksheet)
    x = dr.find_data_salesforce(google_spreadsheet, index)
    y = dr.find_data_fxrone(google_spreadsheet, index)
    assert x == pygsheets.worksheet
    assert y == pygsheets.worksheet

    def mockreturn_fail_worksheet(str, num):
        raise IndexError

    monkeypatch.setattr(google_spreadsheet, "worksheet", mockreturn_fail_worksheet)
    with pytest.raises(er.InvalidArgumentError):
        assert dr.find_data_salesforce(google_spreadsheet, index)
        assert dr.find_data_fxrone(google_spreadsheet, index)

    def mockreturn_fail_worksheet(str, num):
        raise pygsheets.InvalidArgumentValue

    monkeypatch.setattr(google_spreadsheet, "worksheet", mockreturn_fail_worksheet)
    with pytest.raises(er.InvalidArgumentError):
        assert dr.find_data_salesforce(google_spreadsheet, index)
        assert dr.find_data_fxrone(google_spreadsheet, index)

    def mockreturn_fail_worksheet(str, num):
        raise pygsheets.WorksheetNotFound

    monkeypatch.setattr(google_spreadsheet, "worksheet", mockreturn_fail_worksheet)
    with pytest.raises(er.CollectSpreadsheetError):
        assert dr.find_data_salesforce(google_spreadsheet, index)
        assert dr.find_data_fxrone(google_spreadsheet, index)

    def mockreturn_fail_worksheet(str, num):
        raise pygsheets.PyGsheetsException

    monkeypatch.setattr(google_spreadsheet, "worksheet", mockreturn_fail_worksheet)
    with pytest.raises(errors.DiscrepancyReportError):
        assert dr.find_data_salesforce(google_spreadsheet, index)
        assert dr.find_data_fxrone(google_spreadsheet, index)

    def mockreturn_fail_worksheet(str, num):
        raise ValueError

    monkeypatch.setattr(google_spreadsheet, "worksheet", mockreturn_fail_worksheet)
    with pytest.raises(er.SheetsError):
        assert dr.find_data_salesforce(google_spreadsheet, index)
        assert dr.find_data_fxrone(google_spreadsheet, index)