import pygsheets

import anomalyreport.conversion
import discrepancyreport.formating
from discrepancyreport import discrepancy_report as dr, errors
from sheets import general as go, errors as err
import pytest
import pandas as pd

from anomalyreport import data_conversion


def test_authorize_exception():
    with pytest.raises(err.SheetsError):
        go.authorizing()


def test_open_spreadsheet_exception():
    link = "1/2/3/4/5/6/7"
    with pytest.raises(err.SheetsError):
        go.collect_spreadsheet(pygsheets.client.Client, link)


def test_open_worksheet_exception():
    with pytest.raises(err.SheetsError):
        go.collect_worksheet(pygsheets.Spreadsheet, 0)


def test_cannot_make_object_exception():
    column_name = ['Account Name', 'First Name', 'Last Name', 'Mailing Street', 'Mailing City',
                   'Mailing State/Province (text only)', 'Mailing Zip/Postal Code', 'Mailing Country (text only)',
                   'Phone', 'Mobile', 'Email', 'Account ID', 'Contact ID', 'Finaxar User ID', 'Finaxar Company ID',
                   'Finaxar Organization ID', 'Company Registration Id', 'Opportunity Name', 'Stage']
    data = [['Acme', 'Andrico', 'Cahyadi', '45 Jalan Pinang', 'Singapore', '', '200200', 'Singapore', '6599887766',
             '6511223344', 'info@acme.com', '00000', '00001', '', '', '123', '', '']]
    df = pd.DataFrame(data, columns=column_name)
    for index, row in df.iterrows():
        with pytest.raises(err.InvalidColumnCountError):
            anomalyreport.conversion.object_creation_from_dataframe(row)


def test_invalid_column_exception():
    header = [
        "Wrong Header 1",
        "Account",
        "Wrong Header 2",
        "Mailing Street",
        "Mailing City",
        "Mailing Zip/Postal Code",
        "Mailing Country (text only)",
        "Phone",
        "Mobile",
        "Finaxar Organization ID",
        "Wrong Header 3",
    ]
    d = pd.DataFrame(columns=header)
    with pytest.raises(errors.InvalidColumnNameError):
        assert discrepancyreport.formating.format_salesforce(d)

    columns = [
        "Irrelevant Field 1",
        "Irrelevant Field 2",
        "address",
        "city",
        "zip_code",
        "country",
        "phone",
        "id",
        "Irrelevant Field 3",
    ]
    d = pd.DataFrame(columns=columns)
    with pytest.raises(errors.InvalidColumnNameError):
        assert discrepancyreport.formating.format_fxrone(d)

    data = {
        "another_field": [
            "Company 1",
            " Company2 ",
            "Company&3",
            "Company & 4",
            "Company - 5",
            "Company6-",
            "Company.7",
            "Company 8.",
            "Company& 9.- ",
        ],
        "other_fields": [
            "field1",
            "field2",
            "field 3",
            "field4",
            "field5",
            "field6",
            "field7 ",
            "field8",
            "field9",
        ],
    }
    d = pd.DataFrame(data)
    with pytest.raises(errors.InvalidColumnNameError):
        assert discrepancyreport.formating.create_df_key_column(d)
