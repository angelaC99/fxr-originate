import pytest
import pandas as pd
import numpy as np
from pandas.util.testing import assert_frame_equal

import discrepancyreport.creating
import discrepancyreport.differences
import discrepancyreport.formating
from discrepancyreport import discrepancy_report as dr, errors
import pygsheets


# unit tests for functions that are similar
# TODO: Combine functions that are similar into one python file?
def test_check_confirmation():
    reply = "Y"
    assert dr.check_confirmation(reply) is None

    reply = "y"
    assert dr.check_confirmation(reply) is None

    reply = "."
    with pytest.raises(SystemExit):
        dr.check_confirmation(reply)

    reply = "n"
    with pytest.raises(SystemExit):
        dr.check_confirmation(reply)

    reply = ""
    with pytest.raises(SystemExit):
        dr.check_confirmation(reply)


@pytest.mark.skip(reason="Test not working")
def test_gen_discrepancyreport(monkeypatch):
    google_spreadsheet = pygsheets.Spreadsheet
    setting = 0

    def mockreturn_add_worksheet(str):
        setting = 1

    def mockreturn_worksheet_by_title(str):
        return pygsheets.Worksheet

    monkeypatch.setattr(
        google_spreadsheet, "worksheet_by_title", mockreturn_worksheet_by_title
    )
    monkeypatch.setattr(google_spreadsheet, "add_worksheet", mockreturn_add_worksheet)
    x = dr.gen_discrepancyreport(google_spreadsheet)
    assert x == pygsheets.Worksheet

    def mockreturn_fail_worksheet_by_title(num):
        if num == 1:
            discrepancyreport = pygsheets.Worksheet
            return discrepancyreport
        else:
            raise FileNotFoundError

    monkeypatch.setattr(
        google_spreadsheet,
        "worksheet_by_title",
        mockreturn_fail_worksheet_by_title(setting),
    )
    with pytest.raises(FileNotFoundError):
        assert dr.gen_discrepancyreport(google_spreadsheet)


def test_gen_df_x(monkeypatch):
    google_spreadsheet = pygsheets.Worksheet
    test = pd.DataFrame({"col1": [1, 2], "col2": [3, 4]})

    def mockreturn_get_as_df(include_tailing_empty=False):
        d = pd.DataFrame({"col1": [1, 2], "col2": [3, 4]})
        return d

    monkeypatch.setattr(google_spreadsheet, "get_as_df", mockreturn_get_as_df)
    x = dr.gen_df_fxrone(google_spreadsheet)
    y = dr.gen_df_salesforce(google_spreadsheet)
    assert x.equals(test)
    assert y.equals(test)

    test = pd.DataFrame({"col1": [1, 3], "col3": [np.nan, 6]})

    def mockreturn_fail_get_as_df(include_tailing_empty=False):
        d = pd.DataFrame({"col1": [1, np.nan, 3], "col3": [np.nan, np.nan, 6]})
        return d

    monkeypatch.setattr(google_spreadsheet, "get_as_df", mockreturn_fail_get_as_df)
    x = dr.gen_df_fxrone(google_spreadsheet)
    y = dr.gen_df_salesforce(google_spreadsheet)
    assert (
        assert_frame_equal(
            test.reset_index(drop=True), x.reset_index(drop=True), check_dtype=False
        )
        is None
    )
    assert (
        assert_frame_equal(
            test.reset_index(drop=True), y.reset_index(drop=True), check_dtype=False
        )
        is None
    )


def test_format_salesforce():
    test = ["name", "address", "city", "zip_code", "country", "phone", "mobile", "id"]
    test_df = pd.DataFrame(columns=test)

    # Correct Dataframe produced
    columns = [
        "Wrong Header 1",
        "Account Name",
        "Wrong Header 2",
        "Mailing Street",
        "Mailing City",
        "Mailing Zip/Postal Code",
        "Mailing Country (text only)",
        "Phone",
        "Mobile",
        "Finaxar Organization ID",
        "Wrong Header 3",
    ]
    d = pd.DataFrame(columns=columns)
    x = discrepancyreport.formating.format_salesforce(d)
    assert sorted(x.columns) == sorted(test_df.columns)

    # Dataframe does not include "name" field
    columns = [
        "Wrong Header 1",
        "Wrong Header 2",
        "Mailing Street",
        "Mailing City",
        "Mailing Zip/Postal Code",
        "Mailing Country (text only)",
        "Phone",
        "Mobile",
        "Finaxar Organization ID",
        "Wrong Header 3",
    ]
    d = pd.DataFrame(columns=columns)
    with pytest.raises(errors.InvalidColumnNameError):
        assert discrepancyreport.formating.format_salesforce(d)

    # Dataframe that does not include other fields but "name"
    test = ["name", "city", "zip_code", "country", "phone", "mobile"]
    test_df = pd.DataFrame(columns=test)
    columns = [
        "Wrong Header 1",
        "Account Name",
        "Wrong Header 2",
        "Mailing City",
        "Mailing Zip/Postal Code",
        "Mailing Country (text only)",
        "Phone",
        "Mobile",
        "Wrong Header 3",
    ]
    d = pd.DataFrame(columns=columns)
    x = discrepancyreport.formating.format_salesforce(d)
    assert sorted(x.columns) == sorted(test_df.columns)


def test_format_fxrone():
    test = ["name", "address", "city", "zip_code", "country", "phone", "id"]
    test_df = pd.DataFrame(columns=test)

    # Correct Dataframe produced
    columns = [
        "Irrelevant Field 1",
        "name",
        "Irrelevant Field 2",
        "address",
        "city",
        "zip_code",
        "country",
        "phone",
        "id",
        "Irrelevant Field 3",
    ]
    d = pd.DataFrame(columns=columns)
    x = discrepancyreport.formating.format_fxrone(d)
    assert sorted(x.columns) == sorted(test_df.columns)

    # Dataframe produced without "name" field
    columns = [
        "Irrelevant Field 1",
        "Irrelevant Field 2",
        "address",
        "city",
        "zip_code",
        "country",
        "phone",
        "id",
        "Irrelevant Field 3",
    ]
    d = pd.DataFrame(columns=columns)
    with pytest.raises(errors.InvalidColumnNameError):
        assert discrepancyreport.formating.format_fxrone(d)

    # Dataframe produced without other fields
    test = ["name", "city", "zip_code", "country", "phone"]
    test_df = pd.DataFrame(columns=test)
    columns = [
        "Irrelevant Field 1",
        "name",
        "Irrelevant Field 2",
        "city",
        "zip_code",
        "country",
        "phone",
        "Irrelevant Field 3",
    ]
    d = pd.DataFrame(columns=columns)
    x = discrepancyreport.formating.format_fxrone(d)
    assert sorted(x.columns) == sorted(test_df.columns)


def test_create_key():
    data = {
        "name": [
            "Company 1",
            " Company2 ",
            "Company&3",
            "Company & 4",
            "Company - 5",
            "Company6-",
            "Company.7",
            "Company 8.",
            "Company& 9.- ",
        ],
        "other_fields": [
            "field1",
            "field2",
            "field 3",
            "field4",
            "field5",
            "field6",
            "field7 ",
            "field8",
            "field9",
        ],
    }
    test_data = {
        "name": [
            "Company 1",
            " Company2 ",
            "Company&3",
            "Company & 4",
            "Company - 5",
            "Company6-",
            "Company.7",
            "Company 8.",
            "Company& 9.- ",
        ],
        "other_fields": [
            "field1",
            "field2",
            "field 3",
            "field4",
            "field5",
            "field6",
            "field7 ",
            "field8",
            "field9",
        ],
        "key": [
            "Company1",
            "Company2",
            "Companyand3",
            "Companyand4",
            "Company5",
            "Company6",
            "Company7",
            "Company8",
            "Companyand9",
        ],
    }
    test = pd.DataFrame(test_data, columns=["name", "other_fields", "key"])
    d = pd.DataFrame(data, columns=["name", "other_fields"])
    x = discrepancyreport.formating.create_df_key_column(d)
    assert x.equals(test)

    # if field 'name' is missing from the dataframe
    data = {
        "another_field": [
            "Company 1",
            " Company2 ",
            "Company&3",
            "Company & 4",
            "Company - 5",
            "Company6-",
            "Company.7",
            "Company 8.",
            "Company& 9.- ",
        ],
        "other_fields": [
            "field1",
            "field2",
            "field 3",
            "field4",
            "field5",
            "field6",
            "field7 ",
            "field8",
            "field9",
        ],
    }
    d = pd.DataFrame(data, columns=["another_field", "other_fields"])
    with pytest.raises(errors.InvalidColumnNameError):
        assert discrepancyreport.formating.create_df_key_column(d)


@pytest.mark.skip(reason="Cant seem to recreate dataframe accurately")
def test_concat_df():
    data_test = {
        ("name", "FXRDataBase"): {"uniquekey": "a"},
        ("name", "SalesForce"): {"uniquekey": "a"},
        ("address", "FXRDataBase"): {"uniquekey": "b"},
        ("address", "SalesForce"): {"uniquekey": "b"},
        ("city", "FXRDataBase"): {"uniquekey": "c"},
        ("city", "SalesForce"): {"uniquekey": "c"},
        ("zip_code", "FXRDataBase"): {"uniquekey": "d"},
        ("zip_code", "SalesForce"): {"uniquekey": "d"},
        ("country", "FXRDataBase"): {"uniquekey": "e"},
        ("country", "SalesForce"): {"uniquekey": "e"},
        ("id", "FXRDataBase"): {"uniquekey": "f"},
        ("id", "SalesForce"): {"uniquekey": "f"},
        ("phone", "FXRDataBase"): {"uniquekey": "g"},
        ("phone", "SalesForce"): {"uniquekey": "g"},
        ("mobile", "SalesForce"): {"uniquekey": "h"},
    }

    test = pd.DataFrame(data_test)

    data_fxrone = {
        "key": ["uniquekey"],
        "name": ["a"],
        "address": ["b"],
        "city": ["c"],
        "zip_code": ["d"],
        "country": ["e"],
        "id": ["f"],
        "phone": ["g"],
    }
    data_salesforce = {
        "key": ["uniquekey"],
        "name": ["a"],
        "address": ["b"],
        "city": ["c"],
        "zip_code": ["d"],
        "country": ["e"],
        "id": ["f"],
        "phone": ["g"],
        "mobile": ["h"],
    }
    df_fxrone = pd.DataFrame(
        data_fxrone,
        columns=[
            "key",
            "name",
            "address",
            "city",
            "zip_code",
            "country",
            "id",
            "phone",
        ],
    )
    df_salesforce = pd.DataFrame(
        data_salesforce,
        columns=[
            "key",
            "name",
            "address",
            "city",
            "zip_code",
            "country",
            "id",
            "phone",
            "mobile",
        ],
    )
    x = discrepancyreport.creating.concat_df(df_salesforce, df_fxrone)
    assert x.equals(test)

    data_fxrone = {
        "key": ["uniquekey"],
        "name": ["a"],
        "address": ["b"],
        "city": ["c"],
        "zip_code": ["d"],
        "country": ["e"],
        "id": ["f"],
        "phone": ["g"],
    }
    data_salesforce = {
        "key": ["uniquekey", "uniquekey"],
        "name": ["a", "a1"],
        "address": ["b", "b1"],
        "city": ["c", "c1"],
        "zip_code": ["d", "d1"],
        "country": ["e", "e1"],
        "id": ["f", "f1"],
        "phone": ["g", "g1"],
        "mobile": ["h", "h1"],
    }
    df_fxrone = pd.DataFrame(
        data_fxrone,
        columns=[
            "key",
            "name",
            "address",
            "city",
            "zip_code",
            "country",
            "id",
            "phone",
        ],
    )
    df_salesforce = pd.DataFrame(
        data_salesforce,
        columns=[
            "key",
            "name",
            "address",
            "city",
            "zip_code",
            "country",
            "id",
            "phone",
            "mobile",
        ],
    )
    with pytest.raises(errors.DRRepeatNamesError):
        assert discrepancyreport.creating.concat_df(df_salesforce, df_fxrone)

    data_salesforce = {
        "key": ["uniquekey"],
        "name": ["a"],
        "address": ["b"],
        "city": ["c"],
        "zip_code": ["d"],
        "country": ["e"],
        "id": ["f"],
        "phone": ["g"],
        "mobile": ["h", "h1"],
    }
    data_fxrone = {
        "key": ["uniquekey", "uniquekey"],
        "name": ["a", "a1"],
        "address": ["b", "b1"],
        "city": ["c", "c1"],
        "zip_code": ["d", "d1"],
        "country": ["e", "e1"],
        "id": ["f", "f1"],
        "phone": ["g", "g1"],
    }
    df_fxrone = pd.DataFrame(
        data_fxrone,
        columns=[
            "key",
            "name",
            "address",
            "city",
            "zip_code",
            "country",
            "id",
            "phone",
        ],
    )
    df_salesforce = pd.DataFrame(
        data_salesforce,
        columns=[
            "key",
            "name",
            "address",
            "city",
            "zip_code",
            "country",
            "id",
            "phone",
            "mobile",
        ],
    )
    with pytest.raises(errors.DRRepeatNamesError):
        assert discrepancyreport.creating.concat_df(df_salesforce, df_fxrone)


def test_left_merge_fxrone_format_notfound_fxrone():
    test_data2 = {
        "Name": {0: "Ac"},
        "Address": {0: "Bc"},
        "City": {0: "Cc"},
        "Zip_Code": {0: "Dc"},
        "Country": {0: "Ec"},
        "ID": {0: "Fc"},
        "Phone": {0: "Gc"},
        "Mobile": {0: "Hc"},
    }
    test2 = pd.DataFrame(
        test_data2,
        columns=[
            "Name",
            "Address",
            "City",
            "Zip_Code",
            "Country",
            "ID",
            "Phone",
            "Mobile",
        ],
    )

    data_fxrone = {
        "key": ["personA", "personB"],
        "name": ["a", "A"],
        "address": ["b", "B"],
        "city": ["c", "C"],
        "zip_code": ["d", "D"],
        "country": ["e", "E"],
        "id": ["f", "F"],
        "phone": ["g", "G"],
    }
    data_salesforce = {
        "key": ["personA", "personC"],
        "name": ["a", "Ac"],
        "address": ["b", "Bc"],
        "city": ["c", "Cc"],
        "zip_code": ["d", "Dc"],
        "country": ["e", "Ec"],
        "id": ["f", "Fc"],
        "phone": ["g", "Gc"],
        "mobile": ["h", "Hc"],
    }
    df_fxrone = pd.DataFrame(
        data_fxrone,
        columns=[
            "key",
            "name",
            "address",
            "city",
            "zip_code",
            "country",
            "id",
            "phone",
        ],
    )
    df_salesforce = pd.DataFrame(
        data_salesforce,
        columns=[
            "key",
            "name",
            "address",
            "city",
            "zip_code",
            "country",
            "id",
            "phone",
            "mobile",
        ],
    )
    x = discrepancyreport.creating.left_merge_fxrone(df_salesforce, df_fxrone)
    y = discrepancyreport.formating.format_notfound_fxrone(x)
    assert y.to_dict() == test2.to_dict()


def test_right_merge_salesforce_format_notfound_salesforce():
    test_data2 = {
        "Name": {0: "A"},
        "Address": {0: "B"},
        "City": {0: "C"},
        "Zip_Code": {0: "D"},
        "Country": {0: "E"},
        "ID": {0: "F"},
        "Phone": {0: "G"},
    }
    test2 = pd.DataFrame(
        test_data2,
        columns=[
            "Name",
            "Address",
            "City",
            "Zip_Code",
            "Country",
            "ID",
            "Phone"
        ],
    )

    data_fxrone = {
        "key": ["personA", "personB"],
        "name": ["a", "A"],
        "address": ["b", "B"],
        "city": ["c", "C"],
        "zip_code": ["d", "D"],
        "country": ["e", "E"],
        "id": ["f", "F"],
        "phone": ["g", "G"],
    }
    data_salesforce = {
        "key": ["personA", "personC"],
        "name": ["a", "Ac"],
        "address": ["b", "Bc"],
        "city": ["c", "Cc"],
        "zip_code": ["d", "Dc"],
        "country": ["e", "Ec"],
        "id": ["f", "Fc"],
        "phone": ["g", "Gc"],
        "mobile": ["h", "Hc"],
    }
    df_fxrone = pd.DataFrame(
        data_fxrone,
        columns=[
            "key",
            "name",
            "address",
            "city",
            "zip_code",
            "country",
            "id",
            "phone",
        ],
    )
    df_salesforce = pd.DataFrame(
        data_salesforce,
        columns=[
            "key",
            "name",
            "address",
            "city",
            "zip_code",
            "country",
            "id",
            "phone",
            "mobile",
        ],
    )
    x = discrepancyreport.creating.right_merge_salesforce(df_salesforce, df_fxrone)
    y = discrepancyreport.formating.format_notfound_salesforce(x)
    assert y.to_dict() == test2.to_dict()


def test_diff_name():

    v1 = "MICHAEL"
    v2 = "michael"
    assert discrepancyreport.differences.diff_name(v1, v2) is None

    v1 = "michael"
    v2 = "mickey"
    assert discrepancyreport.differences.diff_name(v1, v2) == True

    v1 = "Person s/o Person"
    v2 = "Person s/o Person"
    assert discrepancyreport.differences.diff_name(v1, v2) is None

    v1 = ""
    v2 = ""
    assert discrepancyreport.differences.diff_name(v1, v2) is None

    v1 = "Minnie"
    v2 = ""
    assert discrepancyreport.differences.diff_name(v1, v2) == True


def test_diff_str():
    v1 = "string"
    v2 = "string"
    assert discrepancyreport.differences.diff_str(v1, v2) is None

    v1 = "string"
    v2 = ""
    assert discrepancyreport.differences.diff_str(v1, v2) == True

    v1 = "STRING"
    v2 = "string"
    assert discrepancyreport.differences.diff_str(v1, v2) == True

    v1 = "str-ing"
    v2 = "string"
    assert discrepancyreport.differences.diff_str(v1, v2) == True


def test_diff_city():
    city = {"Singapore": "1880252"}

    v2 = "Singapore"
    v1 = "1880252"
    assert discrepancyreport.differences.diff_city(city, v1, v2) is None

    v2 = "Japan"
    v1 = "1880252"
    assert discrepancyreport.differences.diff_city(city, v1, v2) == True

    v2 = "Singapore"
    v1 = "188"
    assert discrepancyreport.differences.diff_city(city, v1, v2) == True

    v2 = "Singapore"
    v1 = "Singapore"
    assert discrepancyreport.differences.diff_city(city, v1, v2) == True


def test_diff_country():
    country = {"Singapore": "SG"}

    v2 = "Singapore"
    v1 = "SG"
    assert discrepancyreport.differences.diff_country(country, v1, v2) is None

    v2 = "Singapore"
    v1 = "MYN"
    assert discrepancyreport.differences.diff_country(country, v1, v2) == True

    v1 = "Singapore"
    v2 = "Singapore"
    assert discrepancyreport.differences.diff_country(country, v1, v2) == True

    v2 = "Vietnam"
    v1 = "SG"
    assert discrepancyreport.differences.diff_country(country, v1, v2) == True


def test_diff_num():

    v1 = "98765432"
    v2 = "98765432"
    v3 = "98765432"
    assert discrepancyreport.differences.diff_num(v1, v2, v3) is None

    v1 = "98765431"
    v2 = "98765432"
    v3 = "98765432"
    assert discrepancyreport.differences.diff_num(v1, v2, v3) == True

    v1 = "98765432"
    v2 = "98765431"
    v3 = "98765432"
    assert discrepancyreport.differences.diff_num(v1, v2, v3) is None

    v1 = "98765432"
    v2 = "98765432"
    v3 = "98765431"
    assert discrepancyreport.differences.diff_num(v1, v2, v3) is None

    v1 = "98765432"
    v2 = "98765431"
    v3 = "98765433"
    assert discrepancyreport.differences.diff_num(v1, v2, v3) == True

