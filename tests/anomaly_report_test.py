import anomalyreport.conversion
from anomalyreport import anomaly_report as ar, data_conversion, Organization as org
import pandas as pd



def test_object_creation_from_dataframe():
    column_name = ['Account Name', 'First Name', 'Last Name', 'Mailing Street', 'Mailing City',
                   'Mailing State/Province (text only)', 'Mailing Zip/Postal Code', 'Mailing Country (text only)',
                   'Phone', 'Mobile', 'Email', 'Account ID', 'Contact ID', 'Finaxar User ID', 'Finaxar Company ID',
                   'Finaxar Organization ID', 'Company Registration Id', 'Opportunity Name', 'Stage']
    data = [['Acme', 'Andrico', 'Cahyadi', '45 Jalan Pinang', 'Singapore', '', '200200', 'Singapore', '6599887766',
             '6511223344', 'info@acme.com', '00000', '00001', '', '', '123', '', '', 'Closed Lost']]
    df = pd.DataFrame(data, columns=column_name)
    for index, row in df.iterrows():
        func_test = anomalyreport.conversion.object_creation_from_dataframe(index, row)
    org_test = org.Organization(2, 'Acme', 'Andrico', 'Cahyadi', '45 Jalan Pinang', 'Singapore', '', '200200',
                                'Singapore', '6599887766', '6511223344', 'info@acme.com', '00000', '00001', '', '',
                                '123', '', '', 'Closed Lost')

    for org_attr, func_attr in zip(org_test.__dict__.values(), func_test.__dict__.values()):
        assert org_attr == func_attr


def test_list_of_values_creation():
    data = org.Organization(2, 'Acme', 'Andrico', 'Cahyadi', '45 Jalan Pinang', 'Singapore', '', '200200',
                                 'Singapore', '6599887766', '6511223344', 'info@acme.com', '00000A', '00001A', '', '',
                                 '123', '', '', 'Closed Lost')
    func_test_list = anomalyreport.conversion.list_of_values_creation(data)
    val_test_list = ['Acme', 'Andrico', 'Cahyadi', '45 Jalan Pinang', 'Singapore', '', '200200', 'Singapore',
                      '6599887766', '6511223344', 'info@acme.com', '00000A', '00001A', '', '', '123',
                      '', '', 'Closed Lost']
    assert func_test_list == val_test_list


def test_clean_name_for_comparison():
    data = [org.Organization(2, 'Acme-& . pte', 'Andrico', 'Cahyadi', '45 Jalan Pinang', 'Singapore', '', '200200',
                            'Singapore', '6599887766', '6511223344', 'info@acme.com', '00000A', '00001A', '', '',
                            '123', '', '', 'Closed Lost')]

    ar.clean_name_for_comparison(data)
    cleared_name = [org.Organization(2, 'Acmeandpte', 'Andrico', 'Cahyadi', '45 Jalan Pinang', 'Singapore', '',
                                    '200200', 'Singapore', '6599887766', '6511223344', 'info@acme.com', '00000A',
                                    '00001A', '', '', '123', '', '', 'Closed Lost')]
    cleared_name[0].normalised_name = cleared_name[0].acc_name
    for result, test in zip(cleared_name, data):
        assert result.normalised_name == test.normalised_name

"""
def test_check_and_normalize_string():
    func_test_bool, target_string = ar.check_and_normalize_string(True, "")
    assert func_test_bool == False and target_string == ""
    func_test_bool, target_string = ar.check_and_normalize_string(False, "ANDRICO")
    assert func_test_bool == False and target_string == "Andrico"
    func_test_bool, target_string = ar.check_and_normalize_string(True, "ANDRICO123")
    assert func_test_bool == False and target_string == "Andrico123"
    func_test_bool, target_string = ar.check_and_normalize_string(False, "ANDRICO123")
    assert func_test_bool == True and target_string == "Andrico"
    func_test_bool, target_string = ar.check_and_normalize_string(False, "Andrico")
    assert func_test_bool == False and target_string == "Andrico"


def test_check_and_normalize_number():
    func_test_bool, target_num = ar.check_and_normalize_number(True, "")
    assert func_test_bool == False and target_num == ""
    func_test_bool, target_num = ar.check_and_normalize_number(False, "123  ")
    assert func_test_bool == False and target_num == "123"
    func_test_bool, target_num = ar.check_and_normalize_number(True, "123  A")
    assert func_test_bool == False and target_num == "123A"
    func_test_bool, target_num = ar.check_and_normalize_number(False, "123 A")
    assert func_test_bool == True and target_num == "123"
    func_test_bool, target_num = ar.check_and_normalize_number(False, "123")
    assert func_test_bool == False and target_num == "123"


def test_check_and_normalize_email():
    func_test_bool, target_email = ar.check_and_suggest_email(True, "")
    assert func_test_bool == False and target_email == ""
    func_test_bool, target_email = ar.check_and_suggest_email(True, "andricofinaxar.com")
    assert func_test_bool == False and target_email == "andricofinaxar.com"
    func_test_bool, target_email = ar.check_and_suggest_email(True, "andrico@finaxar.com")
    assert func_test_bool == False and target_email == "andrico@finaxar.com"
    func_test_bool, target_email = ar.check_and_suggest_email(False, "andrico")
    assert func_test_bool == True and target_email == "Not possible to fix"
    func_test_bool, target_email = ar.check_and_suggest_email(False, "andrico@finaxar.com")
    assert func_test_bool == False and target_email == "andrico@finaxar.com"
"""


def test_find_duplicate_records_below():
    list_to_check = [org.Organization(2, 'Acmeandpte', 'Andrico', 'Cahyadi', '45 Jalan Pinang', 'Singapore', '',
                                      '200200', 'Singapore', '6599887766', '6511223344', 'info@acme.com', '00000A',
                                      '00001A', '', '', '123', '', '', 'Closed Lost'),
                     org.Organization(3, 'Acmeandpte', 'Andrico', 'Cahyadi', '45 Jalan Pinang', 'Singapore', '',
                                      '200200', 'Singapore', '6599887766', '6511223344', 'info@acme.com', '00000A',
                                      '00001A', '', '', '123', '', '', 'Closed Lost'),
                     org.Organization(4, 'Acmeandpte', 'Andrico', 'Cahyadi', '45 Jalan Pinang', 'Singapore', '',
                                      '200200', 'Singapore', '6599887766', '6511223344', 'info@acme.com', '00000A',
                                      '00001A', '', '', '123', '', '', 'Closed Lost')
                     ]
    func_test_list = ar.find_duplicate_record_below(0, list_to_check)
    assert func_test_list == ['3', '4']


def test_check_missing_field():
    column_name = ['Account Name', 'First Name', 'Last Name', 'Mailing Street', 'Mailing City',
                   'Mailing State/Province (text only)', 'Mailing Zip/Postal Code', 'Mailing Country (text only)',
                   'Phone', 'Mobile', 'Email', 'Account ID', 'Contact ID', 'Finaxar User ID', 'Finaxar Company ID',
                   'Finaxar Organization ID', 'Company Registration Id', 'Opportunity Name', 'Stage']
    account = org.Organization(2, 'Acmeandpte', 'Andrico', 'Cahyadi', '45 Jalan Pinang', 'Singapore', '',
                               '200200', 'Singapore', '6599887766', '6511223344', 'info@acme.com', '00000A',
                               '00001A', '', '', '123', '', '', 'Closed Lost')
    account.normalised_name = "Acmeandpte"
    func_test_list = ar.check_missing_field(column_name, account)
    print(func_test_list)
    result_list = ['Mailing State/Province (text only)', 'Finaxar User ID', 'Finaxar Company ID',
                   'Company Registration Id', 'Opportunity Name']
    assert func_test_list == result_list


def test_check_wrong_format():
    header = ['Account Name', 'First Name', 'Last Name', 'Mailing Street', 'Mailing City',
              'Mailing State/Province (text only)', 'Mailing Zip/Postal Code', 'Mailing Country (text only)',
              'Phone', 'Mobile', 'Email', 'Account ID', 'Contact ID', 'Finaxar User ID', 'Finaxar Company ID',
              'Finaxar Organization ID', 'Company Registration Id', 'Opportunity Name', 'Stage']
    source_org = org.Organization(2, 'Acmeandpte', 'ANdrico123', 'Cahyadi', '45 Jalan Pinang', 'Singapore', '',
                                  '200200', 'Singapore', '(65)99887766', '6511223344', 'infoacme.com', '00000A',
                                  '00001A', '', '', '123', '', '', 'Closed Lost')
    normalized_org = org.Organization(2, 'Acme&- .pte', 'ANdrico123', 'Cahyadi', '45 Jalan Pinang', 'Singapore', '',
                                      '200200', 'Singapore', '(65)99887766', '6511223344', 'infoacme.com', '00000A',
                                      '00001A', '', '', '123', '', '', 'Closed Lost')
    missing_column = ['Mailing State/Province (text only)', 'Finaxar User ID', 'Finaxar Company ID',
                      'Company Registration Id', 'Opportunity Name']

    func_wrong_format_field, func_field_changes, func_normalized_org = ar.check_wrong_format(
        header, source_org, normalized_org, missing_column)

    result_wrong_format_field = ['First Name', 'Phone', 'Email']
    result_field_changes = {'First Name': ('ANdrico123', 'Andrico'), 'Phone': ('(65)99887766', '6599887766'),
                            'Email': ('infoacme.com', 'Not possible to fix')}
    result_normalized_org = org.Organization(2, 'Acme&- .pte', 'Andrico', 'Cahyadi', '45 Jalan Pinang', 'Singapore', '',
                                             '200200', 'Singapore', '6599887766', '6511223344', 'infoacme.com',
                                             '00000A', '00001A', '', '', '123', '', '', 'Closed Lost')
    assert func_wrong_format_field == result_wrong_format_field
    assert func_field_changes == result_field_changes
    assert object.__eq__(func_normalized_org, result_normalized_org)
    for x, var in enumerate(func_normalized_org.__dict__):
        assert func_normalized_org.__getattribute__(var) == result_normalized_org.__getattribute__(var)
