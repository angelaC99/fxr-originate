def fix_string(input_string):
    target_string = "".join([j for j in input_string if not j.isdigit()])
    return target_string


def fix_number(input_number):
    target_number = "".join([j for j in input_number if j.isdigit()])
    return target_number