def check_string(input_string):
    wrong = False
    if any(i.isdigit() for i in input_string):
        wrong = True
    return wrong


def check_number(input_string):
    wrong = False
    if not input_string.replace(" ", "").isnumeric():
        wrong = True
    return wrong


def check_identifier(input_string, identifier):
    wrong = False
    if identifier not in input_string:
        wrong = True
    return wrong