from datetime import datetime
from time import time

from sheets import general


def anomaly_report_generation(google_spreadsheet, final_report):
    # Getting the number of rows used
    final_row = final_report.anomaly_total_len() + 20

    # Opening the target sheet
    target_sheet = general.access_and_create_worksheet(google_spreadsheet, "Anomaly Report", final_row)

    # For clearing current sheet
    general.clear_worksheet(target_sheet, final_row)

    # Print title and timestamp
    timestamp = datetime.fromtimestamp(time()).strftime("%Y-%m-%d %H:%M:%S")
    general.print_heading_bold(target_sheet, 1, 1, "Anomaly report", 22)
    general.print_heading_bold(target_sheet, 2, 1, timestamp, 10)
    # Print missing values
    general.print_report(target_sheet, "Missing data", "No of records with missing data",
                         final_report.make_missing_report(), final_report.missing_len(), 4)

    # Printing the wrong format
    general.print_report(target_sheet, "Wrong format data", "No of records with wrong format",
                         final_report.make_wrong_format_report(), final_report.wrong_len(),
                         8 + final_report.missing_len())

    # Printing the duplicates
    general.print_report(target_sheet, "Duplicate data", "No of duplicate data", final_report.make_duplicate_report(),
                         final_report.duplicate_len(),
                         12 + final_report.missing_len() + final_report.wrong_len())


def changelog_report_generation(google_spreadsheet, final_report):
    # Changelog report
    # For clearing current sheet
    worksheet = general.access_and_create_worksheet(google_spreadsheet, "Auto-normalization changelog",
                                                    final_report.make_fix_report().shape[0] + 10)
    general.clear_worksheet(worksheet, final_report.make_fix_report().shape[0] + 10)

    # Printing title, timestamp, and data
    general.print_heading_bold(worksheet, 1, 1, "Auto-normalization changelog", 22)
    timestamp = datetime.fromtimestamp(time()).strftime("%Y-%m-%d %H:%M:%S")
    general.print_heading_bold(worksheet, 2, 1, timestamp, 10)
    general.print_report(worksheet, "Data normalise", "No of record changed", final_report.make_fix_report(),
                         final_report.fix_report_len(), 4)


def normalized_report_generation(google_spreadsheet, final_report):
    # Print Normalised data
    worksheet = general.access_and_create_worksheet(
        google_spreadsheet, "Normalized SalesForceReport", final_report.make_normalized_report().shape[0]+10)
    general.clear_worksheet(worksheet, final_report.make_normalized_report().shape[0] + 10)

    general.print_data(worksheet, final_report.make_normalized_report(), 1, 1)
