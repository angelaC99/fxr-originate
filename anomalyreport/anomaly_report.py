from anomalyreport import Report

from anomalyreport.checking import check_string, check_number, check_identifier
from anomalyreport.fix import fix_string, fix_number
from anomalyreport.normalise import normalize_string, normalize_number

EMAIL_IDENTIFIER = "@"
DOMAINS = [
    "google.com",
    "gmail.com",
    "emaildomain.com",
    "comcast.net",
    "facebook.com",
    "msn.com",
    "yahoo.com",
    "hotmail.com",
    "rocketmail.com",
]
SECOND_LEVEL_DOMAINS = ("yahoo", "hotmail", "google", "mail", "live", "outlook")
TOP_LEVEL_DOMAINS = (
    "co.uk",
    "co.sg",
    "com",
    "com.sg",
    "org",
    "info",
    "fr",
    "co",
    "co.au",
    "au",
    "ph",
    "sg",
)


def find_duplicate_record_below(current_index, list_to_check):
    # Find duplicates on the list
    duplicate_list = []
    for x in list_to_check[current_index + 1:]:
        if list_to_check[current_index].acc_name == x.acc_name:
            duplicate_list.append(str(x.line_no))
    return duplicate_list


def check_missing_field(column_name, account):
    # Check what field are missing from a record, and append it to the list
    missing_column = []
    for column_no, column in enumerate(account.__dict__):
        if not account.__dict__.get(column):
            missing_column.append(column_name[column_no - 2])
    return missing_column


def check_wrong_format(header, source_org, normalized_org, missing_column):
    # Check all the format
    wrong_field = []
    format_change = {}

    if header[1] not in missing_column:
        if check_string(source_org.f_name):
            wrong_field.append(header[1])
            normalized_org.f_name = fix_string(source_org.f_name)
            normalized_org.f_name = normalize_string(normalized_org.f_name)
            format_change[header[1]] = (source_org.f_name, normalized_org.f_name)
    normalized_org.f_name = normalize_string(normalized_org.f_name)

    if header[2] not in missing_column:
        if check_string(source_org.l_name):
            wrong_field.append(header[2])
            normalized_org.l_name = fix_string(source_org.l_name)
            normalized_org.l_name = normalize_string(normalized_org.l_name)
            format_change[header[2]] = (source_org.l_name, normalized_org.l_name)
    normalized_org.l_name = normalize_string(normalized_org.l_name)

    if header[8] not in missing_column:
        if check_number(source_org.phone):
            wrong_field.append(header[8])
            new_num = fix_number(source_org.phone)
            if new_num == "":
                format_change[header[8]] = (source_org.phone, "Not possible to fix")
            else:
                new_num = normalize_number(new_num)
                format_change[header[8]] = (source_org.phone, new_num)
                normalized_org.phone = normalize_number(new_num)
    normalized_org.phone = normalize_number(normalized_org.phone)

    if header[9] not in missing_column:
        if check_number(source_org.mobile):
            wrong_field.append(header[9])
            new_num = fix_number(source_org.mobile)
            if new_num == "":
                format_change[header[9]] = (source_org.mobile, "Not possible to fix")
            else:
                new_num = normalize_number(new_num)
                format_change[header[9]] = (source_org.mobile, new_num)
                normalized_org.mobile = normalize_number(new_num)
    normalized_org.mobile = normalize_number(normalized_org.mobile)

    if header[10] not in missing_column:
        if check_identifier(source_org.email, EMAIL_IDENTIFIER):
            wrong_field.append(header[10])
            format_change[header[10]] = (source_org.f_name, "Not possible to fix")

    if header[15] not in missing_column:
        if check_number(source_org.finaxar_org_id):
            wrong_field.append(header[15])
            new_num = fix_number(source_org.finaxar_org_id)
            if new_num == "":
                format_change[header[15]] = (source_org.finaxar_org_id, "Not possible to fix")
            else:
                new_num = normalize_number(new_num)
                format_change[header[15]] = (source_org.finaxar_org_id, new_num)
                normalized_org.finaxar_org_id = normalize_number(new_num)
    normalized_org.finaxar_org_id = normalize_number(normalized_org.finaxar_org_id)

    return wrong_field, format_change, normalized_org


def clean_name_for_comparison(sales_force_list):
    # Remove name inconsistencies
    for org in sales_force_list:
        org.normalised_name = org.acc_name.replace("-", "").replace("&", "and").replace(" ", "").replace(".", "")


def anomaly(column_head, sales_force_list, normalized_org_list):

    # Anomaly report
    # Getting the source worksheet

    # Dictionary to document everything
    missing_records = {}
    wrong_format_records = {}
    duplicate_records = {}
    record_changes_made = {}

    for index, (sales_force_org, normalized_org) in enumerate(zip(sales_force_list, normalized_org_list)):
        duplicate_line_no_field = [str(sales_force_org.line_no)]
        missing_field = check_missing_field(column_head, sales_force_org)

        wrong_format_field, field_changes, normalized_org = check_wrong_format(
            column_head, sales_force_org, normalized_org, missing_field)

        if len(field_changes) != 0:
            record_changes_made[sales_force_org] = field_changes
        if sales_force_org not in duplicate_records:
            duplicate_line_no_field.extend(find_duplicate_record_below(index, sales_force_list))

        if len(missing_field) != 0:
            missing_records[normalized_org_list[index]] = missing_field
        if len(wrong_format_field) != 0:
            wrong_format_records[normalized_org_list[index]] = wrong_format_field
        if len(duplicate_line_no_field) != 1:
            duplicate_records[normalized_org_list[index]] = duplicate_line_no_field
    final_report = Report.Report(missing_records, wrong_format_records, duplicate_records,
                                 record_changes_made, normalized_org_list, column_head)
    return final_report


