import pandas as pd


class Report:
    def __init__(self, missing_dict, wrong_dict, duplicate_dict, fix_dict, normalized_list, data_header):
        self.missing_dict = missing_dict
        self.wrong_dict = wrong_dict
        self.duplicate_dict = duplicate_dict
        self.fix_dict = fix_dict
        self.normalized_list = normalized_list
        self.header = data_header

    def make_missing_report(self):
        missing_data = {
            "Line no": [item.line_no for item in self.missing_dict],
            "Company Name": [item.acc_name for item in self.missing_dict],
            "Number of missing field": [len(value) for value in self.missing_dict.values()],
            "Missing field": ["(" + ", ".join(value) + ")" for value in self.missing_dict.values()]}
        missing_dataframe = pd.DataFrame(missing_data)
        return missing_dataframe

    def missing_len(self):
        return len(self.missing_dict)

    def make_wrong_format_report(self):
        wrong_format_data = {
            "Line no": [item.line_no for item in self.wrong_dict],
            "Company Name": [item.acc_name for item in self.wrong_dict],
            "Number of wrong format field": [len(value) for value in self.wrong_dict.values()],
            "Wrong format field": ["(" + ", ".join(value) + ")" for value in self.wrong_dict.values()]}
        wrong_format_dataframe = pd.DataFrame(wrong_format_data)
        return wrong_format_dataframe

    def wrong_len(self):
        return len(self.wrong_dict)

    def make_duplicate_report(self):
        duplicate_data = {
            "Account name": [item.acc_name for item in self.duplicate_dict],
            "Number of duplicates": [len(value) for value in self.duplicate_dict.values()],
            "Duplicates on line": ["(" + ", ".join(value) + ")" for value in self.duplicate_dict.values()]}
        duplicate_dataframe = pd.DataFrame(duplicate_data)
        return duplicate_dataframe

    def duplicate_len(self):
        return len(self.duplicate_dict)

    def make_fix_report(self):
        fix_dataframe_list = []
        for record, fields in self.fix_dict.items():
            for field, value in fields.items():
                fix_dataframe_list.append([record, field, value[0], value[1]])
        column_head = ["Account name", "Field", "Original", "Changes"]
        fix_dataframe = pd.DataFrame(fix_dataframe_list, columns=column_head)
        return fix_dataframe

    def fix_report_len(self):
        return len(self.fix_dict)

    def make_normalized_report(self):
        normalized_dataframe_list = []
        # Change list of object into list of values and make it into dataframe
        for org in self.normalized_list:
            from anomalyreport.conversion import list_of_values_creation
            org_in_value = list_of_values_creation(org)
            normalized_dataframe_list.append(org_in_value)
        normalized_dataframe = pd.DataFrame(normalized_dataframe_list, columns=self.header)
        return normalized_dataframe

    def normalized_len(self):
        return len(self.normalized_list)

    def anomaly_total_len(self):
        return self.missing_len() + self.wrong_len() + self.duplicate_len()

