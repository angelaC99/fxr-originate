from anomalyreport import Organization as SalesForceOrg
from anomalyreport import anomaly_error_handling


def object_creation_from_dataframe(index, row):
    try:
        organisation = SalesForceOrg.Organization(index + 2, row['Account Name'], row['First Name'], row['Last Name'],
                                                  row['Mailing Street'], row['Mailing City'],
                                                  row['Mailing State/Province (text only)'],
                                                  row['Mailing Zip/Postal Code'], row['Mailing Country (text only)'],
                                                  str(row['Phone']), str(row['Mobile']), row['Email'],
                                                  row['Account ID'], row['Contact ID'], row['Finaxar User ID'],
                                                  row['Finaxar Company ID'],
                                                  str(int(float(row['Finaxar Organization ID']))),
                                                  row['Company Registration Id'], row['Opportunity Name'], row['Stage'])
        return organisation
    except Exception:
        raise anomaly_error_handling.InvalidColumnCountError


def list_of_values_creation(org):
    # Creating the list of values from list of organisation
    org_in_list = [
                    org.acc_name,
                    org.f_name,
                    org.l_name,
                    org.mail_st,
                    org.mail_city,
                    org.mail_state,
                    org.post_code,
                    org.country,
                    org.phone,
                    org.mobile,
                    org.email,
                    org.acc_id,
                    org.contact_id,
                    org.finaxar_user_id,
                    org.finaxar_company_id,
                    org.finaxar_org_id,
                    org.company_reg_id,
                    org.opp_name,
                    org.stage,
                ]

    return org_in_list