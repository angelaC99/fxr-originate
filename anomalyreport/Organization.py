class Organization:
    def __init__(
        self,
        line_no,
        acc_name,
        f_name,
        l_name,
        mail_st,
        mail_city,
        mail_state,
        post_code,
        country,
        phone,
        mobile,
        email,
        acc_id,
        contact_id,
        finaxar_user_id,
        finaxar_company_id,
        finaxar_org_id,
        company_reg_id,
        opp_name,
        stage,
    ):
        self.line_no: int = line_no
        self.normalised_name: str = ""
        self.acc_name: str = acc_name
        self.f_name: str = f_name
        self.l_name: str = l_name
        self.mail_st: str = mail_st
        self.mail_city: str = mail_city
        self.mail_state: str = mail_state
        self.post_code: str = post_code
        self.country: str = country
        self.phone: str = phone
        self.mobile: str = mobile
        self.email: str = email
        self.acc_id: str = acc_id
        self.contact_id: str = contact_id
        self.finaxar_user_id: str = finaxar_user_id
        self.finaxar_company_id: str = finaxar_company_id
        self.finaxar_org_id: str = finaxar_org_id
        self.company_reg_id: str = company_reg_id
        self.opp_name: str = opp_name
        self.stage: str = stage

    def __repr__(self):
        return self.acc_name

