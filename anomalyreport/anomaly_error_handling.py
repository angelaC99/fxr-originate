from sheets.errors import SheetsError


class AnomalyReportError(SheetsError):
    def __init__(self):
        self.error_code = 500
        self.error_message = "Unknown Error in Anomaly Report has occurred"


class InvalidColumnCountError(AnomalyReportError):
    def __init__(self):
        self.error_code = 501
        self.error_message = """
        Spreadsheet with invalid column number has been found.
        Please read documentation and follow the correct format or check if correct spreadsheet is inputted"""
