from errors import OriginateError

class SheetsError(OriginateError):
    def __init__(self, error=None):
        self.error = error
        self.error_code = 400
        self.error_message = "Unknown Error has occurred: {}".format(error)


class ClientFileNotFoundError(SheetsError):
    def __init__(self, error=None):
        self.error = error
        self.error_code = 404
        self.error_code = "File is not found, please check the client secret: {}".format(error)


class CollectSpreadsheetError(SheetsError):
    def __init__(self, link=None):
        self.link = link
        self.error_code = 401
        self.error_message = "Unknown Error: Cannot parse spreadsheet with link: {}".format(link)


class SpreadsheetNoValidURLKey(CollectSpreadsheetError):
    def __init__(self, link=None):
        self.link = link
        self.error_code = 401
        self.error_message = "Specified Spreadsheet does not exist"


class IndexError(CollectSpreadsheetError):
    def __init__(self, link = None):
        self.link = link
        self.error_code = 401
        self.error_message = "Index Error (Link has lesser than 4 '/' hence key is not passed)"


class CollectWorksheetError(SheetsError):
    def __init__(self, index=None):
        self.index = index
        self.error_code = 402
        self.error_message = "Sheet with index '{}' is not found".format(index)


class InvalidArgumentError(CollectWorksheetError):
    def __init__(self, index=None):
        self.index = index
        self.error_code = 402
        self.error_message = "Invalid argument has been input: {}".format(index)


class InvalidClientKey(SheetsError):
    def __init__(self):
        self.error_code = 403
        self.error_message = """"
        The google API key is not found. Please check if:
            1) Google API key is in the same folder as this script.
            2) Google API key is renamed to "client_secret.json"
        Please visit this link for the instructions on setting up the API key.
        https://support.google.com/googleapi/answer/6158862?hl=en"""
