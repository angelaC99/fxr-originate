import pygsheets

from discrepancyreport import errors
from sheets import errors as err


def authorizing():
    try:
        gc = pygsheets.authorize()
        return gc
    except pygsheets.AuthenticationError:
        raise err.InvalidClientKey
    except OSError as e:
        raise err.ClientFileNotFoundError(e)
    except pygsheets.PyGsheetsException as e:
        raise errors.DiscrepancyReportError(e)
    except Exception as e:
        raise err.SheetsError(e)


def collect_spreadsheet(google_client, link):
    try:
        key = link.split("/")[5]
        spreadsheet = google_client.open_by_key(key)
        return spreadsheet
    except IndexError:
        raise err.IndexError(link)
    except pygsheets.NoValidUrlKeyFound:
        raise err.SpreadsheetNoValidURLKey
    except pygsheets.SpreadsheetNotFound:
        raise err.CollectSpreadsheetError
    except pygsheets.PyGsheetsException as e:
        raise errors.DiscrepancyReportError(e)
    except Exception as e:
        raise err.SheetsError(e)


def collect_worksheet(google_spreadsheet, index):
    try:
        worksheet = google_spreadsheet.worksheet("index", index)
        return worksheet
    except IndexError:
        raise err.InvalidArgumentError(index)
    except pygsheets.InvalidArgumentValue:
        raise err.InvalidArgumentError(index)
    except pygsheets.WorksheetNotFound:
        raise err.CollectSpreadsheetError(index)
    except pygsheets.PyGsheetsException as e:
        raise errors.DiscrepancyReportError(e)
    except Exception as e:
        raise err.SheetsError(e)


def access_and_create_worksheet(google_spreadsheet, worksheet_title, last_row=1000):
    try:
        worksheet = google_spreadsheet.worksheet_by_title(worksheet_title)  # Opening the target worksheet
    except pygsheets.exceptions.WorksheetNotFound:
        google_spreadsheet.add_worksheet(worksheet_title, rows=last_row)
        # Create the target worksheet if it does not exist
        worksheet = google_spreadsheet.worksheet_by_title(worksheet_title)
    return worksheet


def get_cleaned_worksheet_value(worksheet):     # Getting all the value and header
    # Getting non empty values
    all_data = worksheet.get_all_values(include_tailing_empty_rows=False, include_tailing_empty=False)
    header = all_data.pop(0)  # Removing the header
    # Removing the trailing newline (Not necessary when data is inputted properly)
    for i, line in enumerate(all_data):
        for j, col in enumerate(line):
            line[j] = col.strip()
    return header, all_data


def get_cleaned_worksheet_values(worksheet):
    dataframe = worksheet.get_as_df(include_tailing_empty=False, has_header=True).dropna(axis=0, how="all")
    return dataframe


def clear_worksheet(worksheet, last_row):
    # Remove all values and format until the last row
    worksheet.clear()
    range_auto_all = worksheet.get_values("A1", "Y" + str(last_row), returnas="range")
    range_auto_all.unlink()
    range_auto_all.apply_format(worksheet.cell("Z1"))


def print_heading_bold(worksheet, row, column, text, size=10):
    # Printing title or headings
    cell = worksheet.cell((row, column))
    cell.value = text
    cell.text_format["bold"] = True
    cell.text_format["fontSize"] = size
    cell.update()


def print_report(target_sheet, title, heading, dataframe, count, row, col=1):
    print_heading_bold(target_sheet, row, col, title, 16)
    print_heading_bold(target_sheet, row+1, col, "{} = {}".format(heading, str(count)), 10)
    print_data(target_sheet, dataframe, row+2, col)


def print_data(worksheet, dataframe, row, column):
    # Print the dataframe
    rng = worksheet.get_values((row, column), (row, int(dataframe.shape[1])), returnas='range')
    rng.unlink()
    model_cell = worksheet.cell((row, column))
    model_cell.color = (0.7, 0.7, 0.7, 1.0)
    model_cell.set_text_format('bold', True)
    rng.apply_format(model_cell)
    worksheet.set_dataframe(dataframe, (row, column))


def parse_data(google_spreadsheet, index_of_sheet):
    source_sheet = collect_worksheet(google_spreadsheet, index_of_sheet)
    dataframe = get_cleaned_worksheet_values(source_sheet)
    column_head = list(dataframe.columns.values)
    for index, row in dataframe.iterrows():
        for col in column_head:
            stripped = str(row[col]).strip()
            dataframe.loc[index, col] = stripped
    return column_head, dataframe
