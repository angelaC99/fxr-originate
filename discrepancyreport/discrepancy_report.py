from time import time
from datetime import datetime

from discrepancyreport.creating import (
    concat_df,
    left_merge_fxrone,
    right_merge_salesforce,
)
from discrepancyreport.differences import find_diff
from discrepancyreport.formating import (
    format_salesforce,
    format_fxrone,
    create_df_key_column,
    format_notfound_fxrone,
    format_notfound_salesforce,
)
from sheets import general


def getting_dataframes(fxr_index, sales_index, worksheet):
    salesforce_spreadsheet = general.collect_worksheet(worksheet, sales_index)
    fxrone_spreadsheet = general.collect_worksheet(worksheet, fxr_index)

    salesforce_report = general.get_cleaned_worksheet_values(salesforce_spreadsheet)
    fxrone_data = general.get_cleaned_worksheet_values(fxrone_spreadsheet)
    return salesforce_report, fxrone_data


def formatting_dataframes(salesforce_report, fxrone_data):
    salesforce_dataframe = format_salesforce(salesforce_report)
    fxrone_dataframe = format_fxrone(fxrone_data)

    return salesforce_dataframe, fxrone_dataframe


def creating_discrepancies(salesforce_dataframe, fxrone_dataframe):
    salesforce_dataframe = create_df_key_column(salesforce_dataframe)
    fxrone_dataframe = create_df_key_column(fxrone_dataframe)

    concat_data = concat_df(salesforce_dataframe, fxrone_dataframe)

    not_in_fxrone = left_merge_fxrone(salesforce_dataframe, fxrone_dataframe)
    not_in_fxrone = format_notfound_fxrone(not_in_fxrone)

    not_in_salesforce = right_merge_salesforce(salesforce_dataframe, fxrone_dataframe)
    not_in_salesforce = format_notfound_salesforce(not_in_salesforce)

    return concat_data, not_in_fxrone, not_in_salesforce


def set_up_spreadsheet(worksheet, concat_data, not_in_fxrone, not_in_salesforce):
    final_row = len(not_in_salesforce) + len(not_in_fxrone) + len(concat_data)
    discrepancy_spreadsheet = general.access_and_create_worksheet(
        worksheet, "Discrepancy report", final_row + 20
    )
    general.clear_worksheet(discrepancy_spreadsheet, final_row + 20)

    return discrepancy_spreadsheet


def gen_header_details(discrepancy_spreadsheet):
    general.print_heading_bold(discrepancy_spreadsheet, 1, 1, "Discrepancy report", 22)
    timestamp = datetime.fromtimestamp(time()).strftime("%Y-%m-%d %H:%M:%S")
    general.print_heading_bold(discrepancy_spreadsheet, 2, 1, timestamp, 10)


def gen_discrepancy(discrepancy_sheet, df_concat):
    general.print_heading_bold(discrepancy_sheet, 4, 1, "Differences in Data", 16)

    general.print_heading_bold(
        discrepancy_sheet,
        5,
        1,
        "Differences between SalesForce and FXROne Database are highlighted in yellow",
    )
    general.print_heading_bold(
        discrepancy_sheet,
        6,
        1,
        "Please cater to all discrepancies and ensure data are identical",
    )

    rng = discrepancy_sheet.get_values("A7", "P8", returnas="range")
    rng.unlink()
    model_cell = discrepancy_sheet.cell("A7")
    model_cell.color = (0.7, 0.7, 0.7, 1.0)
    model_cell.set_text_format("bold", True)
    rng.apply_format(model_cell)

    discrepancy_sheet.set_dataframe(df_concat, (7, 1), copy_index=True)
    find_diff(discrepancy_sheet, df_concat)


def gen_not_in_fxrone(discrepancy_spreadsheet, not_in_fxrone, concat_data):
    general.print_report(
        discrepancy_spreadsheet,
        "Not Found in FXROne",
        "Number of organisation(s) not found in FXROne Database "
        "(check if name is correctly input)",
        not_in_fxrone,
        len(not_in_fxrone),
        11 + len(concat_data),
    )


def gen_not_in_salesforce(discrepancy_spreadsheet, not_in_salesforce, concat_data, not_in_fxrone):
    general.print_report(
        discrepancy_spreadsheet,
        "Not Found in SalesForce",
        "Number of organisation(s) not found in SalesForce Report "
        "(check if name is correctly input)",
        not_in_salesforce,
        len(not_in_salesforce),
        17 + len(concat_data) + len(not_in_fxrone),
    )


#TODO: Same function as main (can be removed)
def run_program(fxr_index, sales_index, link):
    key = general.authorizing()

    worksheet = general.collect_spreadsheet(key, link)

    salesforce_report, fxrone_data, worksheet = getting_dataframes(fxr_index, sales_index, worksheet)

    salesforce_dataframe, fxrone_dataframe = formatting_dataframes(salesforce_report, fxrone_data)

    concat_data, not_in_fxrone, not_in_salesforce = creating_discrepancies(salesforce_dataframe, fxrone_dataframe)

    discrepancy_spreadsheet = set_up_spreadsheet(worksheet, concat_data, not_in_fxrone, not_in_salesforce)

    gen_header_details(discrepancy_spreadsheet)

    gen_discrepancy(discrepancy_spreadsheet, concat_data)

    gen_not_in_fxrone(discrepancy_spreadsheet, not_in_fxrone, concat_data)

    gen_not_in_salesforce(discrepancy_spreadsheet, not_in_salesforce, concat_data, not_in_fxrone)




