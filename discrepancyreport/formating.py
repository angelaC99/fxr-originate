from discrepancyreport import errors


def format_salesforce(df_salesforce):
    df_salesforce = df_salesforce.filter(
        {
            "Account Name",
            "Mailing Street",
            "Mailing City",
            "Mailing Zip/Postal Code",
            "Mailing Country (text only)",
            "Phone",
            "Mobile",
            "Finaxar Organization ID",
        }
    ).rename(
        columns={
            "Account Name": "name",
            "Mailing Street": "address",
            "Mailing City": "city",
            "Mailing Zip/Postal Code": "zip_code",
            "Mailing Country (text only)": "country",
            "Phone": "phone",
            "Mobile": "mobile",
            "Finaxar Organization ID": "id",
        }
    )
    if "name" in df_salesforce.columns:
        return df_salesforce
    else:
        raise errors.InvalidColumnNameError


def format_fxrone(df_fxrone):
    df_fxrone = df_fxrone.filter(
        ["name", "address", "city", "zip_code", "country", "phone", "id"]
    )
    if "name" in df_fxrone.columns:
        return df_fxrone
    else:
        raise errors.InvalidColumnNameError


def create_df_key_column(df_salesforce):
    try:
        df_salesforce["key"] = (
            df_salesforce["name"]
                .str.replace(" ", "")
                .str.replace("&", "and")
                .str.replace("-", "")
                .str.replace(".", "")
        )
        return df_salesforce
    except:
        raise errors.InvalidColumnNameError


def format_notfound_fxrone(notfound_fxrone):
    notfound_fxrone = (
        notfound_fxrone.drop(
            [
                "name_y",
                "address_y",
                "city_y",
                "zip_code_y",
                "country_y",
                "phone_y",
                "id_y",
            ],
            axis=1,
        )
            .rename(
            columns={
                "city_x": "City",
                "id_x": "ID",
                "mobile": "Mobile",
                "country_x": "Country",
                "name_x": "Name",
                "zip_code_x": "Zip_Code",
                "phone_x": "Phone",
                "address_x": "Address",
                "key": "Key",
            }
        )
            .reset_index()
    )

    notfound_fxrone = notfound_fxrone[
        ["Name", "Address", "City", "Zip_Code", "Country", "ID", "Phone", "Mobile"]
    ]
    return notfound_fxrone


def format_notfound_salesforce(notfound_salesforce):
    notfound_salesforce = (
        notfound_salesforce.drop(
            [
                "city_y",
                "name_y",
                "country_y",
                "address_y",
                "mobile",
                "zip_code_y",
                "phone_y",
                "id_y",
            ],
            axis=1,
        )
            .rename(
            columns={
                "city_x": "City",
                "id_x": "ID",
                "country_x": "Country",
                "name_x": "Name",
                "zip_code_x": "Zip_Code",
                "phone_x": "Phone",
                "address_x": "Address",
                "key": "Key",
            }
        )
            .reset_index()
    )

    notfound_salesforce = notfound_salesforce[
        ["Name", "Address", "City", "Zip_Code", "Country", "ID", "Phone"]
    ]
    return notfound_salesforce