from sheets.errors import SheetsError


class DiscrepancyReportError(SheetsError):
    def __init__(self, error=None):
        self.error = error
        self.error_code = 600
        self.error_message = "Unknown pygsheets Error in Discrepancy Report has occurred {}".format(error)


class DRRepeatNamesError(DiscrepancyReportError):
    def __init__(self):
        self.error_code = 602
        self.error_message = """
        There are repeat names in SalesForce Report. 
        Please run Anomaly Report first to identify and REMOVE all repeats before running Discrepancy Report.
        """


class InvalidColumnNameError(DiscrepancyReportError):
    def __init__(self):
        self.error_code = 601
        self.error_message = """
        Spreadsheet with invalid column names has been input.
        Please read documentation and follow the correct format or check if correct spreadsheet is input"""