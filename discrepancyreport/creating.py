import numpy as np
import pandas as pd

from discrepancyreport import errors


def concat_df(df_salesforce, df_fxrone):
    try:
        df_concat = pd.concat(
            [df_salesforce.set_index("key"), df_fxrone.set_index("key")],
            axis="columns",
            join="inner",
            keys=["SalesForce", "FXRDataBase"],
            sort=True,
        ).replace(np.nan, "", regex=True)
        df_concat.columns = df_concat.columns.swaplevel(0, 1)
        df_concat = df_concat.sort_index(axis=1)
        df_concat = df_concat[
            ["name", "address", "city", "zip_code", "country", "id", "phone", "mobile"]
        ]
        return df_concat
    except:
        raise errors.DRRepeatNamesError


def left_merge_fxrone(df_salesforce, df_fxrone):
    notfound_fxrone = (
        df_salesforce.merge(df_fxrone, indicator="i", how="outer", on="key")
            .query('i == "left_only"')
            .drop("i", 1)
    )
    return notfound_fxrone


def right_merge_salesforce(df_salesforce, df_fxrone):
    notfound_salesforce = (
        df_fxrone.merge(df_salesforce, indicator="i", how="outer", on="key")
            .query('i == "left_only"')
            .drop("i", 1)
    )
    return notfound_salesforce