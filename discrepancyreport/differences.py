import re

city = {"Singapore": "1880252"}
country = {"Singapore": "SG"}


# Highlight the differences in the DataFrames
def highlight_diff(cell):
    cell.color = (1, 1, 0, 0)


# Running through google sheet to find differences
def find_diff(wks, df_concat):
    for rownum in range(9, len(df_concat) + 9):
        for colnum in range(2, wks.cols + 1):
            if colnum <= 15 and colnum % 2 == 0:
                c1 = wks.cell((rownum, colnum))
                c2 = wks.cell((rownum, colnum + 1))
                v1 = c1.value
                v2 = re.sub(" +", " ", c2.value.strip())
                c1.colour = (1, 1, 1, 1)
                c2.colour = (1, 1, 1, 1)
                if v1 == "" and v2 != "":
                    highlight_diff(c2)
                    highlight_diff(c1)
                elif v2 == "":
                    colnum += 2
                    continue
                if colnum == 2:
                    if diff_name(v1, v2):
                        highlight_diff(c1)
                        highlight_diff(c2)
                elif colnum == 4 or colnum == 8 or colnum == 12:
                    if diff_str(v1, v2):
                        highlight_diff(c1)
                        highlight_diff(c2)
                elif colnum == 6:
                    if diff_city(city, v1, v2):
                        highlight_diff(c1)
                        highlight_diff(c2)
                elif colnum == 10:
                    if diff_country(country, v1, v2):
                        highlight_diff(c1)
                        highlight_diff(c2)
                elif colnum == 14:
                    c3 = wks.cell((rownum, colnum + 2))
                    colnum += 1
                    c3.colour = (1, 1, 1, 1)
                    v3 = re.sub(" +", " ", c2.value.strip())
                    if diff_num(v1, v2, v3):
                        highlight_diff(c1)
                        highlight_diff(c2)
                        highlight_diff(c3)
                colnum += 2


# name comparison
def diff_name(v1, v2):
    if v2.casefold() != v1.casefold():
        return True


# Address, ZIP and ID comparison
def diff_str(v1, v2):
    if v2 != v1:
        return True


# City comparison
def diff_city(city_dict, v1, v2):
    if v2 in city_dict.keys():
        if city_dict[v2] != v1:
            return True
    else:
        return True


# Country comparison
def diff_country(country_dict, v1, v2):
    if v2 in country_dict.keys():
        if country_dict[v2] != v1:
            return True
    else:
        return True


# Phone and mobile comparison
def diff_num(v1, v2, v3):
    if v2 != v1 and v3 != v1:
        return True